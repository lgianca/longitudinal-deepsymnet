# Longitudinal DeepSymNet

This repository contains supporting code for the following manuscript:

> “Quantifying Neurodegenerative Progression with DeepSymNet, an end-to-end data-driven approach”, by Danilo Pena, Arko Barman, Jessika S Suescun, Xiaoqian Jiang, Mya C Schiess and Luca Giancardo, published in “Frontiers in Neuroscience-Brain Imaging Methods” (in press)

## Background and Goal
In this work, we propose a data-driven method based on an extension of a deep learning architecture, DeepSymNet, that identifies longitudinal changes without relying on prior brain regions of interest, an atlas, or non-linear registration steps. Our approach is trained end-to-end and learns how a patient’s brain regional structure dynamically changes between two-time points directly from the raw voxels. This methodology has the potential to enable tools able to quantify neurodegenerative progression in disorders like Alzheimer’s Disease (AD). Please refer to the above paper for a detailed overview of the methods implemented. 

## Results
The final tuned DeepSymNet model performed comparable or better than existing Freesurfer longitudinal and voxel-based pipelines at a fraction of the time needed for preprocessing and predictions. Additionally, we showed that the DeepSymNet was able to generalize a neurodegenerative progression pattern when applied to an external set of high-risk patients known as Mild Cognitive Impairment. These results can be seen in the Jupyter Notebook. Further analysis indicated that the pallidum, putamen, and superior temporal gyrus regions were highly activated brain regions that helped drive the model's prediction. 

# Code

This code uses Python 3.7, conda environments, and Jupyter Notebook. 

### Using conda only

To replicate environment, run:

```python 
conda env create -f logitudinal-deepsymnet.yml
```

That will install the necessary libraries, and activate the environment by running

```python
conda activate longitudinal-deepsymnet
```

### Using pip

```python 
conda create -n longitudinal_deepsymnet python=3.7.3
conda activate longitudinal_deepsymnet
pip install -r requirements.txt 
```

## Files Included

1. DeepSymNet Model Weights and Results
- deepsymnet_model (folder)
2. Model output results to help calculate area under the reciever operating curve 
- model_results (folder)
3. Example data to run through network
- example_brain (folder)
4. Jupyter Notebook & utilities to visualize and compute results
- utils.py
- deepsymnet_results.ipynb

## Notes
An example DeepSymNet application is shown at the end of the deepsymnet_results.ipynb. An example brain was taken from http://www.neuroimagingprimers.org/examples/introduction-primer-example-boxes/, and the same brain was used for both sessions as input to the network.

# Alzheimer’s Disease Neuroimaging Initiative (ADNI)

In order to replicate the full analysis, you will have to request the ADNI dataset (http://adni.loni.usc.edu/). 

Data used in preparation of this article were obtained from the Alzheimer’s Disease Neuroimaging Initiative (ADNI) database (adni.loni.usc.edu). As such, the investigators within the ADNI contributed to the design and implementation of ADNI and/or provided data but did not participate in analysis or writing of this report. A complete listing of ADNI investigators can be found at: http://adni.loni.usc.edu/wp-content/uploads/how_to_apply/ADNI_Acknowledgement_List.pdf

# Other information
This code is free to use for any non-commercial purposes, provided that the original publication is cited. We refer to the original publication for additional information and acknowledgements.

