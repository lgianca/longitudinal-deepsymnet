"""
Utility functions
"""

# import nilearn.image as ni_img
import numpy as np
import pandas as pd

import utils

import sklearn.preprocessing as pre
import nilearn.image as ni_img


# Load the Nifti files:
def loadNifti(fileIn):
    return ni_img.load_img(fileIn)

def loadSubjGt(fileIn, classification):
    """
    retrieve valid subjects from Excel
    :param fileIn:
    :return:
    """

    # patFr = pd.read_csv(fileIn)
    # # validLbl = patFr['Usable Images?'].str.lower() == 'yes'
    # # patFr = patFr[validLbl]
    #
    # # patID
    # patFr['patID'] = patFr['Study ID'].str.lower()
    #
    #
    #
    # # binarize outcome
    # patFr['isStroke'] = (patFr['Stroke?'].str.lower() == 'yes')
    #
    # # fillin NaN
    # patFr.loc[patFr['Stroke Volume'].isna(), 'Stroke Volume'] = 0
    # patFr.loc[patFr['Penumbra Volume'].isna(), 'Penumbra Volume'] = 0

    patFr = pd.read_csv(fileIn)

#     patFr['Subject_Group_Number'] = patFr['Subject_Group_Number'].astype(str)

    # PatID
#     patFr['patID'] = 'sub-' + patFr['Group'].str.lower() + '0' + patFr['Subject_Group_Number']

#     good_registration = np.load('ADNI_good_registration.npy')
#     pairreg = np.load('ADNI_pairreg.npy')

    if classification == 'ad_control':

        # Filter criteria
        lbl = ((patFr.group == 'ad') | (patFr.group == 'cn')) & (patFr.stable == 'stable')
        patFr = patFr[lbl]
        
#         patFr = patFr[patFr['patID'].isin(good_registration)]
#         patFr = patFr[patFr['patID'].isin(pairreg)]

#         print(patFr)
        # Encode labels
        le = pre.LabelEncoder()
        patFr['labels'] = le.fit_transform(patFr['group'])
        
    elif classification == 'multitask':
        
        # Filter criteria
        lbl = (patFr.group == 'ad') | (patFr.group == 'cn') | (patFr.group == 'random') & (patFr.stable == 'stable')
        patFr = patFr[lbl]

        # Encode labels
        le = pre.LabelEncoder()
        patFr['labels'] = le.fit_transform(patFr['group'])
        
    elif classification == 'mci_control_ad':
        
        # Filter criteria
        lbl = ((patFr.group == 'mci') | (patFr.group == 'cn') | (patFr.group == 'ad')) & (patFr.stable == 'stable')
        patFr = patFr[lbl]

        # Encode labels
        le = pre.LabelEncoder()
        patFr['labels'] = le.fit_transform(patFr['group'])
        
    elif classification == 'mci_mmse':
        
        # Filter criteria
        lbl = ((patFr.group_y == 'mci'))
        patFr = patFr[lbl] 

        # Encode labels
        le = pre.LabelEncoder()
        patFr['labels'] = le.fit_transform(patFr['label'])
        
    elif classification == 'mci_stable_not_stable':
        
        # Filter criteria
        lbl = (patFr.group == 'mci') & (patFr.diagSes2 != 1)
        patFr = patFr[lbl] 

        # Encode labels
        le = pre.LabelEncoder()
        patFr['labels'] = le.fit_transform(patFr['stable'])

    else:

        # Encode labels
        le = pre.LabelEncoder()
        patFr['labels'] = le.fit_transform(patFr['group'])

    return patFr



if __name__ == "__main__":
    conf = utils.readConf( 'res/configuration.json' )

    # outDir = c['baseDir'] + '/' + c['dirNifti'] + '/' + patID + '/'



    pass