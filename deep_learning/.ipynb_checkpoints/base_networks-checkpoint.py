# Define base models

from keras.models import Model
from keras.layers import Input, MaxPooling3D, Conv3D, BatchNormalization, Flatten, Lambda, Dropout, Dense, Concatenate
from keras.initializers import RandomNormal, he_normal
from keras import backend as K

# Base CNN with depth 4
# 2 convolutions + 1 Maxpooling at each depth
def base_CNN_d4(input_shape,
        n_filters=16,
        normalization=False,
        double_filters=False,
        dropout=False,
        reduce_dim=False,
        nConv=2,
        normalize_output=False,
        flatten_output=True,
        activation='relu',
        kernel_regularizer=None,
        activity_regularizer=None,
        kernel_size=(3, 3, 3)): # To be shared

    inputs = Input(shape=input_shape)
    print('DEBUG: Base CNN: input shape:', input_shape)

    # Depth 1
    conv1 = Conv3D(filters=n_filters,
                   kernel_size=kernel_size,
                   kernel_initializer=he_normal(),
                   padding='same',
                   activation=activation,
                   input_shape=input_shape,
                   kernel_regularizer=kernel_regularizer,
                   activity_regularizer=activity_regularizer)(inputs)

    # Batch Normalization
    if normalization==True:
        conv1 = BatchNormalization()(conv1)

    # Repeat convolution layers if nConv>1
    count = nConv - 1
    while count > 0:
        conv1 = Conv3D(filters=n_filters,
                   kernel_size=kernel_size,
                   kernel_initializer=he_normal(),
                   padding='same',
                   activation=activation,
                   kernel_regularizer=kernel_regularizer,
                   activity_regularizer=activity_regularizer)(conv1)

        # Batch Normalization
        if normalization:
            conv1 = BatchNormalization()(conv1)
        count -= 1
    print('Output shape of conv1:', conv1._keras_shape)
    # Dropout
    if dropout:
        conv1 = Dropout(0.25)(conv1)
    # Maxpooling
    pool1 = MaxPooling3D(strides=(2, 2, 2), padding='same')(conv1)


    # Depth 2
    if double_filters:
        n_filters *= 2
    conv2 = Conv3D(filters=n_filters,
                   kernel_size=kernel_size,
                   kernel_initializer=he_normal(),
                   padding='same',
                   activation=activation,
                   kernel_regularizer=kernel_regularizer,
                   activity_regularizer=activity_regularizer)(pool1)

    # Batch Normalization
    if normalization==True:
        conv2 = BatchNormalization()(conv2)

    # Repeat convolution layers if nConv>1
    count = nConv - 1
    while count > 0:
        conv2 = Conv3D(filters=n_filters,
                   kernel_size=kernel_size,
                   kernel_initializer=he_normal(),
                   padding='same',
                   activation=activation,
                   kernel_regularizer=kernel_regularizer,
                   activity_regularizer=activity_regularizer)(conv2)

        # Batch Normalization
        if normalization==True:
            conv2 = BatchNormalization()(conv2)
        count -= 1
    print('Output shape of conv2:', conv2._keras_shape)
    # Dropout
    if dropout:
        conv2 = Dropout(0.25)(conv2)
    # Maxpooling
    pool2 = MaxPooling3D(strides=(2, 2, 2), padding='same')(conv2)


    # Depth 3
    if double_filters:
        n_filters *= 2
    conv3 = Conv3D(filters=n_filters,
                   kernel_size=kernel_size,
                   kernel_initializer=he_normal(),
                   padding='same',
                   activation=activation,
                   kernel_regularizer=kernel_regularizer,
                   activity_regularizer=activity_regularizer)(pool2)

    # Batch Normalization
    if normalization==True:
        conv3 = BatchNormalization()(conv3)

    # Repeat convolution layers if nConv>1
    count = nConv - 1
    while count > 0:
        conv3 = Conv3D(filters=n_filters,
                   kernel_size=kernel_size,
                   kernel_initializer=he_normal(),
                   padding='same',
                   activation=activation,
                   kernel_regularizer=kernel_regularizer,
                   activity_regularizer=activity_regularizer)(conv3)
        # Batch Normalization
        if normalization==True:
            conv3 = BatchNormalization()(conv3)
        count -= 1
    print('Output shape of conv3:', conv3._keras_shape)
    # Dropout
    if dropout:
        conv3 = Dropout(0.25)(conv3)

    # Maxpooling
    pool3 = MaxPooling3D(strides=(2, 2, 2), padding='same')(conv3)


    # Depth 4
    if double_filters:
        n_filters *= 2
    conv4 = Conv3D(filters=n_filters,
                   kernel_size=kernel_size,
                   kernel_initializer=he_normal(),
                   padding='same',
                   activation=activation,
                   kernel_regularizer=kernel_regularizer,
                   activity_regularizer=activity_regularizer)(pool3)

    # Batch Normalization
    if normalization==True:
        conv4 = BatchNormalization()(conv4)

    # Repeat convolution layers if nConv>1
    count = nConv - 1
    while count > 0:
        conv4 = Conv3D(filters=n_filters,
                   kernel_size=kernel_size,
                   kernel_initializer=he_normal(),
                   padding='same',
                   activation=activation,
                   kernel_regularizer=kernel_regularizer,
                   activity_regularizer=activity_regularizer)(conv4)

        # Batch Normalization
        if normalization==True:
            conv4 = BatchNormalization()(conv4)
        count -= 1
    print('Output shape of conv4:', conv4._keras_shape)
    # Dropout
    if dropout:
        conv4 = Dropout(0.25)(conv4)

    # Flatten the features
    if flatten_output:
        output = Flatten()(conv4)
    else:
        output = conv4

    # OPTIONAL: Reduce # features
    if reduce_dim:
        output =  Dense(1024,
                        activation=activation,
                        kernel_regularizer=kernel_regularizer,
                        activity_regularizer=activity_regularizer)(output)

    # Normalize output using L2 normalization
    if normalize_output:
        output = Lambda(lambda x: K.l2_normalize(x, axis=1))(output)

    return Model(inputs, output)



def base_CNN_d2(input_shape,
        n_filters=16,
        normalization=False,
        double_filters=False,
        dropout=False,
        reduce_dim=False,
        nConv=2,
        normalize_output=False,
        flatten_output=True,
        activation='relu',
        kernel_regularizer=None,
        activity_regularizer=None,
        kernel_size=(3, 3, 3)): # To be shared

    inputs = Input(shape=input_shape)
    print('DEBUG: Base CNN: input shape:', input_shape)

    # Depth 1
    conv1 = Conv3D(filters=n_filters,
                   kernel_size=kernel_size,
                   kernel_initializer=he_normal(),
                   padding='same',
                   activation=activation,
                   input_shape=input_shape,
                   kernel_regularizer=kernel_regularizer,
                   activity_regularizer=activity_regularizer)(inputs)

    # Batch Normalization
    if normalization==True:
        conv1 = BatchNormalization()(conv1)

    # Repeat convolution layers if nConv>1
    count = nConv - 1
    while count > 0:
        conv1 = Conv3D(filters=n_filters,
                   kernel_size=kernel_size,
                   kernel_initializer=he_normal(),
                   padding='same',
                   activation=activation,
                   kernel_regularizer=kernel_regularizer,
                   activity_regularizer=activity_regularizer)(conv1)

        # Batch Normalization
        if normalization:
            conv1 = BatchNormalization()(conv1)
        count -= 1

    # Dropout
    if dropout:
        conv1 = Dropout(0.25)(conv1)
    # Maxpooling
    pool1 = MaxPooling3D(strides=(2, 2, 2), padding='same')(conv1)


    # Depth 2
    if double_filters:
        n_filters *= 2
    conv2 = Conv3D(filters=n_filters,
                   kernel_size=kernel_size,
                   kernel_initializer=he_normal(),
                   padding='same',
                   activation=activation,
                   kernel_regularizer=kernel_regularizer,
                   activity_regularizer=activity_regularizer)(pool1)

    # Batch Normalization
    if normalization==True:
        conv2 = BatchNormalization()(conv2)

    # Repeat convolution layers if nConv>1
    count = nConv - 1
    while count > 0:
        conv2 = Conv3D(filters=n_filters,
                   kernel_size=kernel_size,
                   kernel_initializer=he_normal(),
                   padding='same',
                   activation=activation,
                   kernel_regularizer=kernel_regularizer,
                   activity_regularizer=activity_regularizer)(conv2)

        # Batch Normalization
        if normalization==True:
            conv2 = BatchNormalization()(conv2)
        count -= 1

    # Dropout
    if dropout:
        conv2 = Dropout(0.25)(conv2)
    # Maxpooling
    pool2 = MaxPooling3D(strides=(2, 2, 2), padding='same')(conv2)

    if flatten_output:
        # Flatten the features
        output = Flatten()(pool2)
    else:
        output = pool2
    # OPTIONAL: Reduce # features
    if reduce_dim:
        output =  Dense(1024,
                        activation=activation,
                        kernel_regularizer=kernel_regularizer,
                        activity_regularizer=activity_regularizer)(output)

    # Normalize output using L2 normalization
    if normalize_output:
        output = Lambda(lambda x: K.l2_normalize(x, axis=1))(output)

    return Model(inputs, output)

def inception_CNN(input_shape,
                    depth=1,
                    use_dropout=False,
                    use_pooling=False,
                    activation='relu',
                    kernel_regularizer = None,
                    activity_regularizer = None): # To be shared

    inputs = Input(shape=input_shape)
    print('DEBUG: Base Inception: input shape:', input_shape)
    
    dilation_rate = 1
    
    filters = 64
    # Inception module
    tower_1 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer, activity_regularizer=activity_regularizer, dilation_rate=dilation_rate, kernel_initializer='random_uniform')(inputs)
    tower_1 = Conv3D(filters, kernel_size=(3, 3, 3), padding='same', activation=activation, kernel_regularizer=kernel_regularizer, activity_regularizer=activity_regularizer, dilation_rate=dilation_rate, kernel_initializer='random_uniform')(tower_1)

    tower_2 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer, activity_regularizer=activity_regularizer, dilation_rate=dilation_rate, kernel_initializer='random_uniform')(inputs)
    tower_2 = Conv3D(filters, kernel_size=(5, 5, 5), padding='same', activation=activation, kernel_regularizer=kernel_regularizer, activity_regularizer=activity_regularizer, dilation_rate=dilation_rate, kernel_initializer='random_uniform')(tower_2)

    tower_3 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(inputs)
    tower_3 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer, activity_regularizer=activity_regularizer, dilation_rate=dilation_rate, kernel_initializer='random_uniform')(tower_3)

    tower_4 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer, activity_regularizer=activity_regularizer, dilation_rate=dilation_rate, kernel_initializer='random_uniform')(inputs)

    merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_3, tower_4])

    if use_dropout:
#         merged_layer = Dropout(0.1)(merged_layer)
        merged_layer = BatchNormalization()(merged_layer)

    if use_pooling:
        merged_layer = MaxPooling3D(strides=(2, 2, 2), padding='same')(merged_layer)
    count = depth - 1

    while count > 0:
        # Inception module
        tower_1 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer, activity_regularizer=activity_regularizer, dilation_rate=dilation_rate, kernel_initializer='random_uniform')(merged_layer)
        tower_1 = Conv3D(filters, kernel_size=(3, 3, 3), padding='same', activation=activation, kernel_regularizer=kernel_regularizer, activity_regularizer=activity_regularizer, dilation_rate=dilation_rate, kernel_initializer='random_uniform')(tower_1)

        tower_2 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer, activity_regularizer=activity_regularizer, dilation_rate=dilation_rate, kernel_initializer='random_uniform')(merged_layer)
        tower_2 = Conv3D(filters, kernel_size=(5, 5, 5), padding='same', activation=activation, kernel_regularizer=kernel_regularizer, activity_regularizer=activity_regularizer, dilation_rate=dilation_rate, kernel_initializer='random_uniform')(tower_2)

        tower_3 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
        tower_3 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer, activity_regularizer=activity_regularizer, dilation_rate=dilation_rate, kernel_initializer='random_uniform')(tower_3)

        tower_4 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation, kernel_regularizer=kernel_regularizer, activity_regularizer=activity_regularizer, dilation_rate=dilation_rate, kernel_initializer='random_uniform')(merged_layer)

        merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_3, tower_4])

        if use_dropout:
#             merged_layer = Dropout(0.1)(merged_layer)
            merged_layer = BatchNormalization()(merged_layer)


        if use_pooling:
            merged_layer = MaxPooling3D(strides=(2, 2, 2), padding='same')(merged_layer)

        count -= 1

    return Model(inputs, merged_layer)
