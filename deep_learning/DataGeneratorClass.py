# Class for data generation
# All data will not fit in the memory
# Inspired from https://stanford.edu/~shervine/blog/keras-how-to-generate-data-on-the-fly.html

import numpy as np
import os
from keras.utils import Sequence, to_categorical
# from scipy.ndimage import zoom
from skimage.transform import resize
import ni_utils

HIST_FEAT_SIZE = 30



# Standard data generator class
class DataGenerator(Sequence):
    'Generates data for Keras'
    def __init__(self, list_IDs, labels, filepath, data_dir, xls_filepath, network_type, normalize=False, batch_size=4, dim=(91, 109, 20), n_channels=1, n_classes=3, shuffle=True):
        'Initialization'
        # print(resample_size)
        self.filepath = filepath
        self.batch_size = batch_size
        self.labels = labels
        self.list_IDs = list_IDs
        self.n_channels = n_channels
        self.n_classes = n_classes
        self.shuffle = shuffle
        # self.rs=resample_size
        self.norm = normalize
        self.data_dir = data_dir
        self.dim = dim
        self.xls_filepath = xls_filepath
        self.network_type = network_type
        self.on_epoch_end()

    def __len__(self):
        'Compute # batches per epoch'
        return int(np.floor(len(self.list_IDs)/self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]

        # Find list of IDs
        list_IDs_temp = [self.list_IDs[k] for k in indexes]

        # Generate data
        [ses01, ses02], y = self.__data_generation(list_IDs_temp)

        return [ses01, ses02], y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.list_IDs))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, list_IDs_temp):
        'Generates data containing batch_size samples' # X : (n_samples, *dim, n_channels)
        # Initialization
        ses01 = np.empty((self.batch_size, *self.dim, self.n_channels))
        ses02 = np.empty((self.batch_size, *self.dim, self.n_channels))

        if self.network_type == 'classification':
            y = np.empty((self.batch_size), dtype=int)
        elif self.network_type == 'regression':
            y = np.empty((self.batch_size))
            # Load xls file
            patFr =  ni_utils.loadSubjGt(self.xls_filepath)

        # Generate data
        for i, patID in enumerate(list_IDs_temp):

            # load file
            FILENAME = self.filepath + '_' + patID + '.npy'
            FILEPATH = os.path.join(self.data_dir, FILENAME)
            brainDict = np.load(FILEPATH)

            # Get hemispheres and resample
            leftBrain = brainDict.item().get('leftBrain')
            # Resample only if specified
            # leftBrain = resize(leftBrain.astype(int), self.rs, anti_aliasing=True, mode='reflect', preserve_range=True)

            rightBrain = brainDict.item().get('rightBrain')
            # Resample only if specified
            # rightBrain = resize(rightBrain.astype(int), self.rs, anti_aliasing=True, mode='reflect', preserve_range=True)

            # normalize
            if self.norm == True:
                leftBrain = (leftBrain - np.mean(leftBrain))/np.std(leftBrain)
                rightBrain = (rightBrain - np.mean(rightBrain))/np.std(rightBrain)

            # Store sample
            ses01[i,] = np.expand_dims(leftBrain, axis=3)
            ses02[i,] = np.expand_dims(rightBrain, axis=3)

            if self.network_type == 'classification':
                # Store class
                y[i] = brainDict.item().get('isStroke')
            elif self.network_type == 'regression':
                # Store stroke volume
                y[i] = patFr.loc[patFr['patID'] == patID, 'Stroke Volume'].iloc[0]

        return [ses01, ses02], y


# Data Generator class with histogram difference features
class DataGeneratorWithHistFeat(Sequence):
    'Generates data for Keras'
    def __init__(self, list_IDs, labels, filepath, dataDir, histFeatFilepath, normalize=False, batch_size=4, dim=(29, 73, 20), n_channels=1, n_classes=2, shuffle=True):
        'Initialization'
        # print(resample_size)
        self.filepath = filepath
        self.batch_size = batch_size
        self.labels = labels
        self.list_IDs = list_IDs
        self.n_channels = n_channels
        self.n_classes = n_classes
        self.shuffle = shuffle
        # self.rs=resample_size
        self.norm = normalize
        self.data_dir = dataDir
        self.dim = dim
        self.histFeatDict = np.load(histFeatFilepath)
        self.on_epoch_end()

    def __len__(self):
        'Compute # batches per epoch'
        return int(np.floor(len(self.list_IDs)/self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]

        # Find list of IDs
        list_IDs_temp = [self.list_IDs[k] for k in indexes]

        # Generate data
        [ses01, ses02, histFeatMat], y = self.__data_generation(list_IDs_temp)

        return [ses01, ses02, histFeatMat], y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.list_IDs))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, list_IDs_temp):
        'Generates data containing batch_size samples' # X : (n_samples, *dim, n_channels)
        # Initialization
        ses01 = np.empty((self.batch_size, *self.dim, self.n_channels))
        ses02 = np.empty((self.batch_size, *self.dim, self.n_channels))
        histFeatMat = np.empty((self.batch_size, HIST_FEAT_SIZE))
        y = np.empty((self.batch_size), dtype=int)

        # Generate data
        for i, patID in enumerate(list_IDs_temp):

            # load file
            FILENAME = self.filepath + '_' + patID + '.npy'
            FILEPATH = os.path.join(self.data_dir, FILENAME)
            brainDict = np.load(FILEPATH)

            # Get hemispheres and resample
            leftBrain = brainDict.item().get('leftBrain')
            # Resample only if specified
            # leftBrain = resize(leftBrain.astype(int), self.rs, anti_aliasing=True, mode='reflect', preserve_range=True)

            rightBrain = brainDict.item().get('rightBrain')
            # Resample only if specified
            # rightBrain = resize(rightBrain.astype(int), self.rs, anti_aliasing=True, mode='reflect', preserve_range=True)

            # normalize
            if self.norm == True:
                leftBrain = (leftBrain - np.mean(leftBrain))/np.std(leftBrain)
                rightBrain = (rightBrain - np.mean(rightBrain))/np.std(rightBrain)

            # Get histogram feature
            histFeat = self.histFeatDict[patID]

            # Store sample
            ses01[i,] = np.expand_dims(leftBrain, axis=3)
            ses02[i,] = np.expand_dims(rightBrain, axis=3)
            histFeatMat[i,] = histFeat

            # Store class
            y[i] = brainDict.item().get('isStroke')

        return [ses01, ses02, histFeatMat], y

####CHANGED FOR PPMI/ADNI DATASET
# Data generator class for multiclass classification
class DataGeneratorMulticlass(Sequence):
    'Generates data for Keras'
    def __init__(self, list_IDs, labels, filepath, data_dir, xls_filepath, normalize=False, batch_size=4, dim=(91, 109, 20), n_channels=1, n_classes=2, shuffle=True):
        'Initialization'
        # print(resample_size)
        self.filepath = filepath
        self.batch_size = batch_size
        self.labels = labels
        self.list_IDs = list_IDs
        self.n_channels = n_channels
        self.n_classes = n_classes
        self.shuffle = shuffle
        # self.rs=resample_size
        self.norm = normalize
        self.data_dir = data_dir
        self.dim = dim
        self.xls_filepath = xls_filepath
        self.on_epoch_end()

    def __len__(self):
        'Compute # batches per epoch'
        return int(np.floor(len(self.list_IDs)/self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]

        # Find list of IDs
        list_IDs_temp = [self.list_IDs[k] for k in indexes]

        # Generate data
        [left, right], y = self.__data_generation(list_IDs_temp)

        return [left, right], y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.list_IDs))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, list_IDs_temp):
        'Generates data containing batch_size samples' # X : (n_samples, *dim, n_channels)
        # Initialization
        ses01 = np.empty((self.batch_size, *self.dim, self.n_channels))
        ses02 = np.empty((self.batch_size, *self.dim, self.n_channels))

        y = np.empty((self.batch_size), dtype=int)

        # Generate data
        for i, patID in enumerate(list_IDs_temp):

            # load file
            FILENAME = str(patID) + '.npy'
            FILEPATH = os.path.join(self.data_dir, FILENAME)
            brainDict = np.load(FILEPATH, allow_pickle=True)

            # Get hemispheres and resample
            ses01_brain = brainDict.item().get('ses01_brain')
            # Resample only if specified
            # leftBrain = resize(leftBrain.astype(int), self.rs, anti_aliasing=True, mode='reflect', preserve_range=True)

            ses02_brain = brainDict.item().get('ses02_brain')
            # Resample only if specified
            # rightBrain = resize(rightBrain.astype(int), self.rs, anti_aliasing=True, mode='reflect', preserve_range=True)

            # normalize
            if self.norm == True:
                ses01_brain = (ses01_brain - np.mean(ses01_brain))/np.std(ses01_brain)
                ses02_brain = (ses02_brain - np.mean(ses02_brain))/np.std(ses02_brain)

            # Store sample
            ses01[i,] = np.expand_dims(ses01_brain, axis=3)
            ses02[i,] = np.expand_dims(ses02_brain, axis=3)

            # Store class
            y[i] = self.labels[patID]

        return [ses01, ses02], to_categorical(y, num_classes=self.n_classes)