# Networks with inception layers

from keras.models import Model
from keras.layers import Input, MaxPooling3D, Conv3D, BatchNormalization, Flatten, Lambda, Dropout, Dense, Concatenate, AveragePooling3D, Subtract
from deep_learning.base_networks import *
from deep_learning.parameters import *

def siameseNetwork_inceptionAfterMerge(input_shape,
                                        depth=2,
                                        n_conv=2,
                                        n_inception=1, # # inception modules
                                        n_filters=16,
                                        normalization=False,
                                        double_filters=False,
                                        dropout=False,
                                        reduce_dim=False,
                                        nConv=2,
                                        normalize_output=False,
                                        flatten_output=True,
                                        activation='relu',
                                        kernel_regularizer=None,
                                        activity_regularizer=None,
                                        kernel_size=(3, 3, 3)):

    print('DEBUG: Simaese Network: input shape:', input_shape)
    if depth == 4:
        base_network = base_CNN_d4(input_shape,
                n_filters=nFilters,
                normalization=batchNormalize,
                double_filters=doubleFilters,
                dropout=useDropout,
                reduce_dim=reduceDim,
                nConv=nConv,
                normalize_output=normalizeOutput,
                flatten_output=flattenOutput,
                activation=activation,
                kernel_regularizer=kernel_regularizer,
                activity_regularizer=activity_regularizer,
                kernel_size=kernel_size)
    elif depth == 2:
        base_network = base_CNN_d2(input_shape,
                n_filters=nFilters,
                normalization=batchNormalize,
                double_filters=doubleFilters,
                dropout=useDropout,
                reduce_dim=reduceDim,
                nConv=nConv,
                normalize_output=normalizeOutput,
                flatten_output=flattenOutput,
                activation=activation,
                kernel_regularizer=kernel_regularizer,
                activity_regularizer=activity_regularizer,
                kernel_size=kernel_size)
    else:
        raise ValueError('Invalid depth'
                '(only 2 and 4 are supported as of now):', depth)

    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))

    # Re-use same instance of base network (weights are shared across both branches)
    processed_left = base_network(input_left)
    processed_right = base_network(input_right)

    # Merging features from two hemispheres
    # l1
    l1_distance_layer = Lambda(lambda tensors: K.abs(tensors[0] - tensors[1]))
    merged_layer = l1_distance_layer([processed_left, processed_right])

    # Inception module
    tower_1 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)
    tower_1 = Conv3D(64, kernel_size=(3, 3, 3), padding='same', activation=activation)(tower_1)

    tower_2 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)
    tower_2 = Conv3D(64, kernel_size=(5, 5, 5), padding='same', activation=activation)(tower_2)

    tower_3 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
    tower_3 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(tower_3)

    merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_3])

    count = n_inception - 1

    while count>0:

        # Inception module
        tower_1 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)
        tower_1 = Conv3D(64, kernel_size=(3, 3, 3), padding='same', activation=activation)(tower_1)

        tower_2 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)
        tower_2 = Conv3D(64, kernel_size=(5, 5, 5), padding='same', activation=activation)(tower_2)

        tower_3 = MaxPooling3D(strides=(2, 2, 2), padding='same')(merged_layer)
        tower_3 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(tower_3)

        merged_layer = Concatenate(axis=1)([tower_1, tower_2, tower_3])
        count -= 1

    # Maxpooling
    poolMerged = MaxPooling3D(strides=(2, 2, 2), padding='same')(merged_layer)

    # Flatten
    poolMerged = Flatten()(poolMerged)

    # Prediction layer
    prediction = Dense(units=1, activation='sigmoid')(poolMerged)

    return Model(inputs=[input_left, input_right], outputs=prediction)

def siameseNetwork_inceptionOnlyMerge(input_shape,
                                        depth=2,
                                        n_inception=1, # # inception modules after merge
                                        activation='relu',
                                        use_dropout=False,
                                        use_pooling_before_merge=False,
                                        use_pooling_after_merge=False,
                                        use_final_pooling=True,
                                        network_type='classification'):

    print('DEBUG: Simaese Network: input shape:', input_shape)
    base_network = inception_CNN(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge)

    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))

    # Re-use same instance of base network (weights are shared across both branches)
    processed_left = base_network(input_left)
    processed_right = base_network(input_right)

    # Merging features from two hemispheres
    # l1
    l1_distance_layer = Lambda(lambda tensors: K.abs(tensors[0] - tensors[1]))
    merged_layer = l1_distance_layer([processed_left, processed_right])

    # Inception module
    tower_1 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)
    tower_1 = Conv3D(64, kernel_size=(3, 3, 3), padding='same', activation=activation)(tower_1)

    tower_2 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)
    tower_2 = Conv3D(64, kernel_size=(5, 5, 5), padding='same', activation=activation)(tower_2)

    tower_3 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
    tower_3 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(tower_3)

    tower_4 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)

    merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_3, tower_4])

    if use_dropout:
        merged_layer = Dropout(0.25)(merged_layer)

    count = n_inception - 1

    while count>0:

        # Maxpooling
        if use_pooling_after_merge:
            merged_layer = MaxPooling3D(strides=(2, 2, 2), padding='same')(merged_layer)

        # Inception module
        tower_1 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)
        tower_1 = Conv3D(64, kernel_size=(3, 3, 3), padding='same', activation=activation)(tower_1)

        tower_2 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)
        tower_2 = Conv3D(64, kernel_size=(5, 5, 5), padding='same', activation=activation)(tower_2)

        tower_3 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
        tower_3 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(tower_3)

        tower_4 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)

        merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_3, tower_4])

        if use_dropout:
            merged_layer = Dropout(0.25)(merged_layer)


        count -= 1

    # Maxpooling
    if use_final_pooling:
        merged_layer = MaxPooling3D(strides=(2, 2, 2), padding='same')(merged_layer)

    # Flatten
    merged_layer = Flatten()(merged_layer)

    # Prediction layer
    if networkType == 'classification':
        prediction = Dense(units=1, activation='sigmoid')(merged_layer)
    elif networkType == 'regression':
        prediction = Dense(units=1)(merged_layer)
    else:
        raise ValueError('Invalid network type'
                '(only classification and regression are supported):', depth)

    return Model(inputs=[input_left, input_right], outputs=prediction)

def siameseNetwork_inceptionOnlyMerge_multiclass(input_shape,
                                        depth=1,
                                        n_inception=1, # # inception modules after merge
                                        activation='relu',
                                        use_dropout=False,
                                        use_pooling_before_merge=False,
                                        use_pooling_after_merge=False,
                                        use_final_pooling=True,
                                        n_classes=2,
                                        kernel_regularizer = None,
                                        activity_regularizer = None):

    print('DEBUG: Simaese Network: input shape:', input_shape)
    base_network = inception_CNN(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge,
                                    kernel_regularizer = kernel_regularizer,
                                    activity_regularizer = activity_regularizer)

    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))

    # Re-use same instance of base network (weights are shared across both branches)
    processed_left = base_network(input_left)
    processed_right = base_network(input_right)
    
    # Maxpooling
    if use_pooling_before_merge:
        processed_left = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_left)
        processed_right = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_right)

    
    # Merging features from two hemispheres
    # l1
    l1_distance_layer = Lambda(lambda tensors: K.abs(tensors[0] - tensors[1]))
    merged_layer = l1_distance_layer([processed_left, processed_right])
    
    filters = 64

    # Inception module
    tower_1 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)
    tower_1 = Conv3D(filters, kernel_size=(3, 3, 3), padding='same', activation=activation)(tower_1)

    tower_2 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)
    tower_2 = Conv3D(filters, kernel_size=(5, 5, 5), padding='same', activation=activation)(tower_2)

    tower_3 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
    tower_3 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation)(tower_3)

    tower_4 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)

    merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_3, tower_4])

#     if use_dropout:
#         merged_layer = Dropout(0.25)(merged_layer)

    count = n_inception - 1

    while count>0:

        # Maxpooling
        if use_pooling_after_merge:
            merged_layer = MaxPooling3D(strides=(2, 2, 2), padding='same')(merged_layer)

        # Inception module
        tower_1 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)
        tower_1 = Conv3D(filters, kernel_size=(3, 3, 3), padding='same', activation=activation)(tower_1)

        tower_2 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)
        tower_2 = Conv3D(filters, kernel_size=(5, 5, 5), padding='same', activation=activation)(tower_2)

        tower_3 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
        tower_3 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation)(tower_3)

        tower_4 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)

        merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_3, tower_4])

#         if use_dropout:
#             merged_layer = Dropout(0.25)(merged_layer)


        count -= 1

    # Maxpooling
    if use_final_pooling:
        merged_layer = MaxPooling3D(strides=(2, 2, 2), padding='same')(merged_layer)
#         merged_layer = AveragePooling3D(strides=(2, 2, 2), padding='same')(merged_layer)

    # Flatten
    merged_layer = Flatten()(merged_layer)

    # Prediction layer
    prediction = Dense(units=n_classes, activation='softmax')(merged_layer)


    return Model(inputs=[input_left, input_right], outputs=prediction)


def siameseNetwork_inceptionOnlyMerge_multiclass_embedding(input_shape,
                                        depth=2,
                                        n_inception=1, # # inception modules after merge
                                        activation='relu',
                                        use_dropout=False,
                                        use_pooling_before_merge=False,
                                        use_pooling_after_merge=False,
                                        use_final_pooling=True,
                                        n_classes=2,
                                        kernel_regularizer = None,
                                        activity_regularizer = None):

    print('DEBUG: Simaese Network: input shape:', input_shape)
    base_network = inception_CNN(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge,
                                    kernel_regularizer = kernel_regularizer,
                                    activity_regularizer = activity_regularizer)

    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))

    # Re-use same instance of base network (weights are shared across both branches)
    processed_left = base_network(input_left)
    processed_right = base_network(input_right)

    # Merging features from two hemispheres
    # l1
    l1_distance_layer = Lambda(lambda tensors: K.abs(tensors[0] - tensors[1]))
    merged_layer = l1_distance_layer([processed_left, processed_right])

    # Inception module
    tower_1 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)
    tower_1 = Conv3D(64, kernel_size=(3, 3, 3), padding='same', activation=activation)(tower_1)

    tower_2 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)
    tower_2 = Conv3D(64, kernel_size=(5, 5, 5), padding='same', activation=activation)(tower_2)

    tower_3 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
    tower_3 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(tower_3)

    tower_4 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)

    merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_3, tower_4])

#     if use_dropout:
#         merged_layer = Dropout(0.25)(merged_layer)

    count = n_inception - 1

    while count>0:

        # Maxpooling
        if use_pooling_after_merge:
            merged_layer = MaxPooling3D(strides=(2, 2, 2), padding='same')(merged_layer)

        # Inception module
        tower_1 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)
        tower_1 = Conv3D(64, kernel_size=(3, 3, 3), padding='same', activation=activation)(tower_1)

        tower_2 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)
        tower_2 = Conv3D(64, kernel_size=(5, 5, 5), padding='same', activation=activation)(tower_2)

        tower_3 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
        tower_3 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(tower_3)

        tower_4 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)

        merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_3, tower_4])

#         if use_dropout:
#             merged_layer = Dropout(0.25)(merged_layer)


        count -= 1

    # Maxpooling
    if use_final_pooling:
        merged_layer = MaxPooling3D(strides=(2, 2, 2), padding='same')(merged_layer)

    # Flatten
    merged_layer = Flatten()(merged_layer)
    
    #Embedding output
    merged_layer = Dense(128, activation=activation)(merged_layer)

    # Prediction layer
    prediction = Dense(units=n_classes, activation='softmax')(merged_layer)
    
    return Model(inputs=[input_left, input_right], outputs=[prediction])

def siameseNetwork_inceptionOnlyMerge_multiclass_max_pool(input_shape,
                                        depth=2,
                                        n_inception=1, # # inception modules after merge
                                        activation='relu',
                                        use_dropout=False,
                                        use_pooling_before_merge=False,
                                        use_pooling_after_merge=False,
                                        use_final_pooling=True,
                                        use_pooling_before_L1=True,
                                        use_pooling_after_L1=False,
                                        n_classes=2,
                                        kernel_regularizer = None,
                                        activity_regularizer = None):

    print('DEBUG: Simaese Network: input shape:', input_shape)
    base_network = inception_CNN(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge,
                                    kernel_regularizer = kernel_regularizer,
                                    activity_regularizer = activity_regularizer)

    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))

    # Re-use same instance of base network (weights are shared across both branches)
    processed_left = base_network(input_left)
    processed_right = base_network(input_right)
    
    # Maxpooling
    if use_pooling_before_L1:
        processed_left = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_left)
        processed_right = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_right)

    # Merging features from two hemispheres
    # l1
    l1_distance_layer = Lambda(lambda tensors: K.abs(tensors[0] - tensors[1]))
    merged_layer = l1_distance_layer([processed_left, processed_right])
    
    # Maxpooling
    if use_pooling_after_L1:
        merged_layer = MaxPooling3D(strides=(2, 2, 2), padding='same')(merged_layer)

    # Inception module
    tower_1 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)
    tower_1 = Conv3D(64, kernel_size=(3, 3, 3), padding='same', activation=activation)(tower_1)

    tower_2 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)
    tower_2 = Conv3D(64, kernel_size=(5, 5, 5), padding='same', activation=activation)(tower_2)

    tower_3 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
    tower_3 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(tower_3)

    tower_4 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)

    merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_3, tower_4])

#     if use_dropout:
#         merged_layer = Dropout(0.25)(merged_layer)

    count = n_inception - 1

    while count>0:

        # Maxpooling
        if use_pooling_after_merge:
            merged_layer = MaxPooling3D(strides=(2, 2, 2), padding='same')(merged_layer)

        # Inception module
        tower_1 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)
        tower_1 = Conv3D(64, kernel_size=(3, 3, 3), padding='same', activation=activation)(tower_1)

        tower_2 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)
        tower_2 = Conv3D(64, kernel_size=(5, 5, 5), padding='same', activation=activation)(tower_2)

        tower_3 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
        tower_3 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(tower_3)

        tower_4 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)

        merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_3, tower_4])

#         if use_dropout:
#             merged_layer = Dropout(0.25)(merged_layer)


        count -= 1

    # Maxpooling
    if use_final_pooling:
        merged_layer = MaxPooling3D(strides=(2, 2, 2), padding='same')(merged_layer)

    # Flatten
    merged_layer = Flatten()(merged_layer)

    # Prediction layer
    prediction = Dense(units=n_classes, activation='softmax')(merged_layer)
    
    return Model(inputs=[input_left, input_right], outputs=[prediction])

# Siamese network using L-1 difference with raw features concatenated
def siameseNetworkL1DiffPlusRawFeatures_inceptionOnlyMerge_multiclass(input_shape,
                                        depth=2,
                                        n_inception=1, # # inception modules after merge
                                        activation='relu',
                                        use_dropout=False,
                                        use_pooling_before_merge=False,
                                        use_pooling_after_merge=False,
                                        use_final_pooling=True,
                                        n_classes=3):

    print('DEBUG: Simaese Network: input shape:', input_shape)
    base_network = inception_CNN(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge)

    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))

    # Re-use same instance of base network (weights are shared across both branches)
    processed_left = base_network(input_left)
    processed_right = base_network(input_right)

    # Merging features from two hemispheres
    # l1
    l1_distance_layer = Lambda(lambda tensors: K.abs(tensors[0] - tensors[1]))
    merged_layer = l1_distance_layer([processed_left, processed_right])
    
    # Concatenate the raw features from the two hemispheres
    merged_layer = Concatenate(axis=-1)([merged_layer, processed_left, processed_right])

    # Inception module
    tower_1 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)
    tower_1 = Conv3D(64, kernel_size=(3, 3, 3), padding='same', activation=activation)(tower_1)

    tower_2 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)
    tower_2 = Conv3D(64, kernel_size=(5, 5, 5), padding='same', activation=activation)(tower_2)

    tower_3 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
    tower_3 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(tower_3)

    tower_4 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)

    merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_3, tower_4])

    if use_dropout:
        merged_layer = Dropout(0.25)(merged_layer)

    count = n_inception - 1

    while count>0:

        # Maxpooling
        if use_pooling_after_merge:
            merged_layer = MaxPooling3D(strides=(2, 2, 2), padding='same')(merged_layer)

        # Inception module
        tower_1 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)
        tower_1 = Conv3D(64, kernel_size=(3, 3, 3), padding='same', activation=activation)(tower_1)

        tower_2 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)
        tower_2 = Conv3D(64, kernel_size=(5, 5, 5), padding='same', activation=activation)(tower_2)

        tower_3 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
        tower_3 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(tower_3)

        tower_4 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)

        merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_3, tower_4])

        if use_dropout:
            merged_layer = Dropout(0.25)(merged_layer)


        count -= 1

    # Maxpooling
    if use_final_pooling:
        merged_layer = MaxPooling3D(strides=(2, 2, 2), padding='same')(merged_layer)

    # Flatten
    merged_layer = Flatten()(merged_layer)

    # Prediction layer
    prediction = Dense(units=n_classes, activation='softmax')(merged_layer)


    return Model(inputs=[input_left, input_right], outputs=prediction)

def siameseNetworkPlusWholeBrain_inceptionOnlyMerge_multiclass(input_shape,
                                                                depth=2,
                                                                n_inception=1, # # inception modules after merge
                                                                activation='relu',
                                                                use_dropout=False,
                                                                use_pooling_before_merge=False,
                                                                use_pooling_after_merge=False,
                                                                use_final_pooling=True,
                                                                n_classes=3):

    print('DEBUG: Simaese Network: input shape:', input_shape)
    base_network = inception_CNN(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge)

    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))
    
#     wholeBrainShapeList = list(input_shape)
#     wholeBrainShapeList[0] = wholeBrainShapeList[0]*2
#     wholeBrainShape = tuple(wholeBrainShapeList)
#     input_wholeBrain = Input(shape=(wholeBrainShape))

    # Re-use same instance of base network (weights are shared across both branches)
    processed_left = base_network(input_left)
    processed_right = base_network(input_right)
    
    # Use pooling across first dimension in whole brain
#     processed_wholeBrain = AveragePooling3D(pool_size = (2, 1, 1), strides=(2, 1, 1), padding='same')(input_wholeBrain)
    # Now use the same representation-learning CNN
    
    l1_distance_layer = Lambda(lambda tensors: K.abs(tensors[0] - tensors[1]))
    processed_wholeBrain = base_network(l1_distance_layer([input_left, input_right]))

    # Merging features from two hemispheres
    # l1
    
    merged_layer = l1_distance_layer([processed_left, processed_right])
    
    # Merge the difference with whole brain features
    merged_layer = Concatenate(axis=-1)([merged_layer, processed_wholeBrain])

    # Inception module
    tower_1 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)
    tower_1 = Conv3D(64, kernel_size=(3, 3, 3), padding='same', activation=activation)(tower_1)

    tower_2 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)
    tower_2 = Conv3D(64, kernel_size=(5, 5, 5), padding='same', activation=activation)(tower_2)

    tower_3 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
    tower_3 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(tower_3)

    tower_4 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)

    merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_3, tower_4])

    if use_dropout:
        merged_layer = Dropout(0.25)(merged_layer)

    count = n_inception - 1

    while count>0:

        # Maxpooling
        if use_pooling_after_merge:
            merged_layer = MaxPooling3D(strides=(2, 2, 2), padding='same')(merged_layer)

        # Inception module
        tower_1 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)
        tower_1 = Conv3D(64, kernel_size=(3, 3, 3), padding='same', activation=activation)(tower_1)

        tower_2 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)
        tower_2 = Conv3D(64, kernel_size=(5, 5, 5), padding='same', activation=activation)(tower_2)

        tower_3 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
        tower_3 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(tower_3)

        tower_4 = Conv3D(64, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)

        merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_3, tower_4])

        if use_dropout:
            merged_layer = Dropout(0.25)(merged_layer)


        count -= 1

    # Maxpooling
    if use_final_pooling:
        merged_layer = MaxPooling3D(strides=(2, 2, 2), padding='same')(merged_layer)

    # Flatten
    merged_layer = Flatten()(merged_layer)

    # Prediction layer
    prediction = Dense(units=n_classes, activation='softmax')(merged_layer)


    return Model(inputs=[input_left, input_right], outputs=prediction)

def siameseNetwork_inceptionOnlyMerge_multiclass_NO_L1(input_shape,
                                        depth=2,
                                        n_inception=1, # # inception modules after merge
                                        activation='relu',
                                        use_dropout=False,
                                        use_pooling_before_merge=False,
                                        use_pooling_after_merge=False,
                                        use_final_pooling=True,
                                        n_classes=2,
                                        kernel_regularizer = None,
                                        activity_regularizer = None,
                                        filters = 64):

    print('DEBUG: Simaese Network: input shape:', input_shape)
    base_network = inception_CNN(input_shape,
                                    depth=depth,
                                    activation=activation,
                                    use_dropout=use_dropout,
                                    use_pooling=use_pooling_before_merge,
                                    kernel_regularizer = kernel_regularizer,
                                    activity_regularizer = activity_regularizer)

    input_left = Input(shape=(input_shape))
    input_right = Input(shape=(input_shape))

    # Re-use same instance of base network (weights are shared across both branches)
    processed_left = base_network(input_left)
    processed_right = base_network(input_right)
    
    # Maxpooling
    if use_pooling_before_merge:
        processed_left = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_left)
        processed_right = MaxPooling3D(strides=(2, 2, 2), padding='same')(processed_right)

    
    # Merging features from two hemispheres
    # l1
#     distance_layer = (lambda tensors: (tensors[0] - tensors[1]))
#     merged_layer = distance_layer([processed_left, processed_right])
    merged_layer = Subtract()([processed_left, processed_right])
    
#     filters = filters

    # Inception module
    tower_1 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)
    tower_1 = Conv3D(filters, kernel_size=(3, 3, 3), padding='same', activation=activation)(tower_1)

    tower_2 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)
    tower_2 = Conv3D(filters, kernel_size=(5, 5, 5), padding='same', activation=activation)(tower_2)

    tower_3 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
    tower_3 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation)(tower_3)

    tower_4 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)

    merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_3, tower_4])

#     if use_dropout:
#         merged_layer = Dropout(0.25)(merged_layer)

    count = n_inception - 1

    while count>0:

        # Maxpooling
        if use_pooling_after_merge:
            merged_layer = MaxPooling3D(strides=(2, 2, 2), padding='same')(merged_layer)

        # Inception module
        tower_1 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)
        tower_1 = Conv3D(filters, kernel_size=(3, 3, 3), padding='same', activation=activation)(tower_1)

        tower_2 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)
        tower_2 = Conv3D(filters, kernel_size=(5, 5, 5), padding='same', activation=activation)(tower_2)

        tower_3 = MaxPooling3D((3, 3, 3), strides=(1, 1, 1), padding='same')(merged_layer)
        tower_3 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation)(tower_3)

        tower_4 = Conv3D(filters, kernel_size=(1, 1, 1), padding='same', activation=activation)(merged_layer)

        merged_layer = Concatenate(axis=4)([tower_1, tower_2, tower_3, tower_4])

#         if use_dropout:
#             merged_layer = Dropout(0.25)(merged_layer)


        count -= 1

    # Maxpooling
    if use_final_pooling:
        merged_layer = MaxPooling3D(strides=(2, 2, 2), padding='same')(merged_layer)
#         merged_layer = AveragePooling3D(strides=(2, 2, 2), padding='same')(merged_layer)

    # Flatten
    merged_layer = Flatten()(merged_layer)

    # Prediction layer
    prediction = Dense(units=n_classes, activation='softmax')(merged_layer)


    return Model(inputs=[input_left, input_right], outputs=prediction)
