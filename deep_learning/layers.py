# Define layers in keras
# Here comes the supremely difficult part!

# Code adapted from https://software.intel.com/en-us/articles/keras-implementation-of-siamese-like-networks

from keras.engine import Layer
from keras import activations
from keras import backend as K
import tensorflow as tf
# import itertools
# from itertools import starmap

class PearsonMergeLayer(Layer):
    '''This defines a Neighborhood Localized Normalized Correlation Layer
    Patch size for calculating cross correlation is 5x5 (default)
    Look only at 3x3x3 neighbors (default)'''

    def __init__(self,
            patch_size=(5, 5, 5),
            dim_ordering='tf', # What is this, huh?
            border_mode='same',
            stride=(1, 1, 1), # Not in use for now
            activation=None,
            similarity='xcorr',
            **kwargs):

        if border_mode != 'same':
            raise ValueError('Invalid border mode for Neighborhood Normalized Correlation Layer'
                    '(only "same" is supported as of now):', border_mode)
        self.kernel_size = patch_size
        self.subsample = stride
        self.dim_ordering = dim_ordering
        self.border_mode = border_mode
        self.activation = activations.get(activation)
        self.epsilon = 0.01
        self.similarity = similarity
        super(PearsonMergeLayer, self).__init__(**kwargs)


    def compute_output_shape(self, input_shape):
        # This one is VERY tricky!
        # 1. Batch size
        # 2. Height
        # 3. Width
        # 4. Depth
        # 5. # filters in previous layer (last value of input shape)
        print('Compute output shape function: Input shape:', input_shape)
        return (input_shape[0])

    # This function does not work, don't ask me why!
    # Calculates Pearson coefficient for 2 lists of patches
    # def PearsonRho(ip1, ip2):
    #
    #     mean1 = map(K.mean, ip1)
    #     mean2 = map(K.mean, ip2)
    #     diffmean1 = map(lambda x, mu_x: x - mu_x, ip1, mean1)
    #     diffmean2 = map(lambda y, mu_y: x - mu_x, ip2, mean2)
    #     sumSqdiffMean1 = map(lambda x: max(self.epsilon, K.sum(K.square(x))), diffmean1) # use max to prevent division by 0
    #     sumSqdiffMean2 = map(lambda y: max(self.epsilon, K.sum(K.square(y))), diffmean2) # use max to prevent division by 0
    #     XCorrCoeff = map(lambda diff_x, diff_y, ssq_dm_x, ssq_dm_y: K.sum(diff_x*diff_y)/(ssq_dm_x*ssq_dm_y), diffmean1, diffmean2, sumSqdiffMean1, sumSqdiffMean2)
    #
    #     return XCorrCoeff

    def call(self, x, mask=None):
        # (Beware! This is where you start understanding nothing at all!)
        # (Proceed at your own risk! Thou hast been warned!)
        input_1, input_2 = x

        # Not in use for now
        # stride_row, stride_col, stride_depth = self.subsample
        inp_shape = K.shape(input_1)
        batch_size = inp_shape[0]

        out_height = inp_shape[1]
        out_width = inp_shape[2]
        out_depth = inp_shape[3]
        out_filt = inp_shape[4]

        output_shape = (batch_size, out_height, out_width, out_depth, out_filt)

        # index adjustor for padding
        # Subtract from and add to current index to get start and end
        indAdj = int(self.kernel_size[0]/2)

        padding = (indAdj, indAdj)
        input_1 = K.spatial_3d_padding(input_1, padding =(padding, padding, padding))
        input_2 = K.spatial_3d_padding(input_2, padding = (padding, padding, padding))
        input1_patches = []
        input2_patches = []
        indices = []
        # Get patches
        # for i in range(offset, inp_shape[0]+offset):
        #     for j in range(offset, inp_shape[1]+offset):
        #         for k in range(offset, inp_shape[2]+offset):
        #             index = (i, j, k)
        #             startPos = (max(i - indAdj, 0),
        #                         max(j - indAdj, 0),
        #                         max(k - indAdj, 0))
        #             endPos = (min(i + indAdj + 1, inp_shape[0]),
        #                         min(j + indAdj + 1, inp_shape[1]),
        #                         min(k + indAdj + 1, inp_shape[2]))
        #             indices.append(index)
        #             input_1_patches.append(K.flatten(input_1[startPos[0]:endPos[0],
        #                                             startPos[1]:endPos[1],
        #                                             startPos[2]:endPos[2]]))
        #             input_2_patches.append(K.flatten(input_2[startPos[0]:endPos[0],
        #                                             startPos[1]:endPos[1],
        #                                             startPos[2]:endPos[2]]))
        #
        # if self.similarity == 'xcorr':
        #     # Compute Pearson coefficient of cross correlation
        #     simMetric = PearsonRho(input_1_patches, input_2_patches)
        # for index, simMet  in zip(indices, simMetric):
        #     output[index] = simMet

        n_pixels = self.kernel_size[0]*self.kernel_size[1]*self.kernel_size[2] # # pixels in volume
        for l in range(inp_shape[4]):
            for i in range(indAdjust, inp_shape[1]+indAdjust):
                for j in range(indAdjust, inp_shape[2]+indAdjust):
                    for k in range(indAdjust, inp_shape[3]+indAdjust):
                        index = (i, j, k, l)
                        indices.append(index)
                        startPos = (i - indAdj, j - indAdj, k - indAdj, l)
                        endPos = (i + indAdj + 1, j + indAdj + 1, k + indAdj + 1, l)
                        input1_patches.append(K.reshape(input_1[:,
                                                            startPos[0]:endPos[0],
                                                            startPos[1]:endPos[1],
                                                            startPos[2]:endPos[2],
                                                            l],
                                                        (-1, 1, n_pixels)))
                        input2_patches.append(K.reshape(input_2[:,
                                                            startPos[0]:endPos[0],
                                                            startPos[1]:endPos[1],
                                                            startPos[2]:endPos[2],
                                                            l],
                                                        (-1, 1, n_pixels)))

        ip1_aggregate = K.concatenate(input1_patches, axis=1)
        ip1_mean = K.mean(ip1_aggregate, axis=-1, keepdims=True)
        ip1_std = K.std(ip1_aggregate, axis=-1, keepdims=True)
        ip1_aggregate = (ip1_aggregate - ip1_mean)/ip1_std

        ip2_aggregate = K.concatenate(input2_patches, axis=1)
        ip2_mean = K.mean(ip2_aggregate, axis=-1, keepdims=True)
        ip2_std = K.std(ip2_aggregate, axis=-1, keepdims=True)
        ip2_aggregate = (ip2_aggregate - ip1_mean)/ip1_std

        pearsonRho = ip1_aggregate*ip2_aggregate/n_pixels

        output = K.reshape(pearsonRho, (-1, otuput_height, output_width, output_depth, output_filt))

        # if self.similarity == 'xcorr':
        #     # Compute Pearson coefficient of cross correlation
        #     simMetric = PearsonRho(input_1_patches, input_2_patches)
        # for index, simMet  in zip(indices, simMetric):
        #     output[index] = simMet
        output = self.activation(output)
        return output

    def get_config(self):
        config = {'patch_size': self.kernel_size,
                'activation': self.activation.__name__,
                'border_mode': self.border_mode,
                'stride': self.subsample,
                'dim_ordering': self.dim_ordering}
        # base_config = super(Correlation_Layer, self).get_config()
        return dict(list(config.items()))


# This is on hold now as it is getting too complex!
# class NeighborhoodNormalizedCorrelationLayer(Layer):
#     '''This defines a Neighborhood Localized Normalized Correlation Layer
#     Patch size for calculating cross correlation is 5x5 (default)
#     Look only at 3x3x3 neighbors (default)'''
#
#     def __init__(self,
#             patch_size=(5, 5, 5),
#             dim_ordering='tf', # What is this, huh?
#             border_mode='same',
#             neighborhood=(3, 3, 3),
#             stride=(5, 5, 5), # In general this will be equal to patch_size
#             activation=None,
#             **kwargs):
#
#         if border_mode != 'valid':
#             raise ValueError('Invalid border mode for Neighborhood Normalized Correlation Layer'
#                     '(only "valid" is supported as of now):', border_mode)
#         self.kernel_size = patch_size
#         self.subsample = stride
#         self.dim_ordering = dim_ordering
#         self.border_mode = border_mode
#         self.neighborhood = neighborhood
#         self.activation = activations.get(activation)
#         super(NeighborhoodNormalizedCorrelationLayer, self).__init__(**kwargs)
#
#
#     def compute_output_shape(self, input_shape):
#         # This one is VERY tricky!
#         # 1. Batch size
#         # 2. Height
#         # 3. Width
#         # 4. Depth
#         # (Beware! This is where you start understanding nothing at all!)
#         # (Proceed at your own risk! Thou hast been warned!)
#         # 5. (Volume of the neighborhood for calculating cross correlation) x (# filters from previous layer)
#         return (input_shape[0][0],
#                 input_shape[0][1],
#                 input_shape[0][2],
#                 input_shape[0][3],
#                 self.neighborhood[0]*self.neighborhood[1]*self.neighborhood[2]*input_shape[0][-1])
#
#     def call(self, x, mask=None):
#         input_1, input_2 = x
#         stride_row, stride_col, stride_depth = self.subsample
#         inp_shape = input_1._keras_shape
#         output_shape = self.compute_output_shape(inp_shape)
#
#         offset = int(self.kernel_size[0]/2)
#         # Compute padding for the two inputs
#         padding_row = (offset, offset)
#         padding_col = (offset, offset)
#         padding_depth = (offset, offset)
#         input_1 = K.spatial_3d_padding(input_1,
#                 padding = (padding_row,
#                         padding_col,
#                         padding_depth))
#         input_2 = K.spatial_3d_padding(input_2,
#                 padding = (padding_row,
#                         padding_col,
#                         padding_depth))
#
#         output_row = output_shape[1]
#         output_col = output_shape[2]
#         output_depth = output_shape[3]
#
#         output = []
#
#         for i in range(offset, inp_shape[0]+offset-1):
#             for j in range(offset, inp_shape[1]+offset-1):
#                 for k in range(offset, inp_shape[2]+offset-1):
#                     indices = []
#                     # Identify the required indices for computing cross-correlation
#                     for x, y, z in itertools.product([i-self.subsample[0], i, i+self.subsample[0]],
#                                                     [j-self.subsample[1], j, j+self.subsample[1]],
#                                                     [k-self.subsample[2], k, k+self.subsample[2]]]):
#                         indices.append((x, y, z))
#
#                     # Get volume of input_1 and reshape to 1d
#                     # Compute mean and std
#
#                     # Get the volumes of from neighborhood in input_2
#                     # Compute mean and std
#
#                     # Compute cross correlation between the volumes
#
#                     # Concatenate
#
#
#
#
#     def get_config(self):
#         config = {'patch_size': self.kernel_size,
#                 'activation': self.activation.__name__,
#                 'border_mode': self.border_mode,
#                 'stride': self.subsample,
#                 'dim_ordering': self.dim_ordering}
#         base_config = super(Correlation_Layer, self).get_config()
#         return dict(list(base_config.items()) + lsit(config.items()))
